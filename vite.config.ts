import { resolve } from "path";
import react from "@vitejs/plugin-react";
import { defineConfig } from "vite";

export default defineConfig({
  build: {
    rollupOptions: {
      output: {
        manualChunks(id) {
          if (id.includes("node_modules")) {
            return id.toString().split("node_modules/")[1].split("/")[0].toString();
          }
        },
      },
    },
    sourcemap: true,
  },
  plugins: [
    react(),
    // chunkSplitPlugin({
    //   customChunk: (args) => {
    //     // files into pages directory is export in single files
    //     let { file } = args;
    //     if (file.startsWith("pages/")) {
    //       file = file.substring(6);
    //       file = file.replace(/\.[^.$]+$/, "");
    //       return file;
    //     }
    //     return null;
    //   },
    //   customSplitting: {
    //     // `react` and `react-dom` will be bundled together in the `react-vendor` chunk (with their dependencies, such as object-assign)
    //     "react-vendor": ["react", "react-dom"],
    //     // Any file that includes `utils` in src dir will be bundled in the `utils` chunk
    //     utils: [/src\/utils/],
    //   },
    //   strategy: "single-vendor",
    // }),
  ],
  resolve: {
    alias: {
      "@components": resolve(__dirname, "./src/components"),
      "@configs": resolve(__dirname, "./src/configs"),
      "@hocs": resolve(__dirname, "./src/hocs"),
      "@hooks": resolve(__dirname, "./src/hooks"),
      "@icons": resolve(__dirname, "./src/icons"),
      "@layouts": resolve(__dirname, "./src/layouts"),
      "@pages": resolve(__dirname, "./pages"),
      "@providers": resolve(__dirname, "./src/providers"),
      "@services": resolve(__dirname, "./src/services"),
    },
  },
  server: {
    host: true,
    open: false,
  },
});
