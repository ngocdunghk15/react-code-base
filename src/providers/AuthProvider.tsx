import { createContext, Fragment, useContext, useEffect, useState } from "react";
import firebaseService from "@services/firebase.service";
import { User } from "firebase/auth";
import { LoadingScreen } from "@components/animation-components/LoadingScreen";
import { Navigate, Outlet, useLocation } from "react-router-dom";

type IAuthProvider = IWrapperElement;

const AuthContext = createContext({});

export const useAuthContext = () => useContext(AuthContext);

export const AuthProvider = ({ children }: IAuthProvider) => {
  const [account, setAccount] = useState<User | unknown>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  useEffect(function onLoad() {
    (async () => {
      await firebaseService
        .onAuthStateChanged()
        .then((userCredential) => {
          console.log({ userCredential });
          setAccount(userCredential);
        })
        .catch((error: any) => {
          console.log({ ErrorMessage: error });
        });
      setIsLoading(false);
    })();
  }, []);

  if (isLoading) {
    return <LoadingScreen />;
  } else {
  }

  return (
    <AuthContext.Provider
      value={{
        account,
        setAccount,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const RequireAuthenticatedRoutes = () => {
  const authValue: any = useAuthContext();
  const location = useLocation();

  if (!authValue?.account) {
    return <Navigate to={{ pathname: "/unauthorized" }} state={{ from: location }} replace />;
  }

  return (
    <Fragment>
      <Outlet />
    </Fragment>
  );
};
