import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { LoadingScreen } from "@components/animation-components/LoadingScreen";
import { RequireAuthenticatedRoutes } from "@providers/AuthProvider";

const AppLayout = React.lazy(() => import("@layouts/AppLayout"));
const AuthLayout = React.lazy(() => import("@layouts/AuthLayout"));
const HomePage = React.lazy(() => import("@pages/app-pages/HomePage"));
const LoginPage = React.lazy(() => import("@pages/auth-pages/LoginPage"));
const LoungePage = React.lazy(() => import("@pages/app-pages/LoungePage"));
const NotFoundPage = React.lazy(() => import("@pages/error-pages/NotFoundPage"));
const UnauthorizedPage = React.lazy(() => import("@pages/error-pages/UnauthorizedPage"));

export const AppRouter = () => {
  return (
    <Suspense fallback={<LoadingScreen />}>
      <Routes>
        <Route path={"/"} element={<AppLayout />}>
          <Route index element={<HomePage />} />
          <Route path={"auth"} element={<AuthLayout />}>
            <Route index element={<LoginPage />} />
            <Route path={"login"} element={<LoginPage />} />
          </Route>
          <Route element={<RequireAuthenticatedRoutes />}>
            <Route path={"/lounge"} element={<LoungePage />} />
          </Route>
          <Route path={"*"} element={<NotFoundPage />} />
          <Route path={"unauthorized"} element={<UnauthorizedPage />} />
        </Route>
      </Routes>
    </Suspense>
  );
};
