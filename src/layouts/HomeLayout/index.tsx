import { Fragment } from "react";

const HomeLayout = ({ children }: IWrapperElement) => {
  return <Fragment>{children}</Fragment>;
};

export default HomeLayout;
