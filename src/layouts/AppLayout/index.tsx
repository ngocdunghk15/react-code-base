import { Fragment } from "react";
import { Outlet } from "react-router-dom";

const AppLayout = () => {
  return (
    <Fragment>
      <Outlet />
    </Fragment>
  );
};

export default AppLayout;
