import React from "react";

const allowedRoles = (listRoles: string[]) => (WrappedComponent: any) => {
  return (props: any) => {
    // const userRoles = useAppSelector(state => state.auth.account.roles);
    const userRoles = ["guest"];
    const checkIsExistRole = (roles: string[]) => {
      let isExist = true;
      listRoles.forEach((role) => {
        if (!roles.includes(role)) isExist = false;
      });
      return isExist;
    };
    if (!checkIsExistRole(userRoles)) return null;
    return <WrappedComponent {...props} />;
  };
};

export default allowedRoles;
