import { OAUTH_REDIRECT_GOOGLE_URL } from "@configs/auth.configs";

import { ApiBaseService } from "./base.service";

export class OAuthService extends ApiBaseService {
  async signInWithGoogle(payload: { authorizationCode: string }) {
    const { data } = await this.httpClient.post("/auth/sso", {
      authorizationCode: payload.authorizationCode,
      grantType: "authorization_code",
      redirectUri: OAUTH_REDIRECT_GOOGLE_URL,
      service: "google",
    });
    return data;
  }

  async connectToGitLab(payload: { redirectUri: string; code: string }) {
    const { data } = await this.httpClient.post(`/gitlab/connect`, payload, {
      isNotRequiredAuthentication: true,
    });
    return data;
  }

  async connectToWakaTime(payload: { redirectUri: string; code: string }) {
    const { data } = await this.httpClient.post(`/wakatime/connect`, payload, {
      isNotRequiredAuthentication: true,
    });
    return data;
  }

  async disconnectGitLab() {
    const { data } = await this.httpClient.post(
      `/gitlab/disconnect`,
      {},
      {
        isNotRequiredAuthentication: true,
      }
    );
    return data;
  }

  async disconnectWakaTime() {
    const { data } = await this.httpClient.post(
      `/wakatime/disconnect`,
      {},
      {
        isNotRequiredAuthentication: true,
      }
    );
    return data;
  }
}

export const oAuthService = new OAuthService();
