import { ApiBaseService } from "./base.service";

export class AuthService extends ApiBaseService {
  public async signUp(signInPayload: { username: string; displayName: string; email: string; password: string; confirmPassword: string }) {
    const { data } = await this.httpClient.post("/auth/register", signInPayload);
    return data;
  }

  public async signIn(signInPayload: { loginField: string; password: string }) {
    const { data } = await this.httpClient.post("/auth/login", signInPayload);
    return data;
  }

  public async getAccountInfo({ token }: { token: string }) {
    const { data } = await this.httpClient.get("/accounts/info", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return data;
  }

  public updateAuthConfigs(authConfigs: IAuthConfigs) {
    this.httpClient.setAuthConfigs(authConfigs);
  }
}

export const authService = new AuthService();
