import { AxiosRequestConfig, InternalAxiosRequestConfig } from 'axios'

export type DocsList = Array<{ name: string; url: string }>
export interface IHttpRequestConfig extends AxiosRequestConfig {
  isNotRequiredAuthentication?: boolean
}
export interface ICustomInternalAxiosRequestConfig
  extends InternalAxiosRequestConfig {
  isNotRequiredAuthentication?: boolean
}
