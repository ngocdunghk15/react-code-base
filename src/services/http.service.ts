import type { AxiosError, AxiosInstance, AxiosResponse } from "axios";
import axios from "axios";
import {
  ICustomInternalAxiosRequestConfig,
  IHttpRequestConfig,
} from "@services/types/types";

export default class Http {
  private instance: AxiosInstance;
  private authConfigs: IAuthConfigs;

  constructor(config?: IHttpRequestConfig) {
    const axiosConfigs = config || {};
    const instance = axios.create({ ...axiosConfigs });
    Object.assign(instance, this.setupInterceptorsTo(instance));
    this.authConfigs = {
      accessToken: null,
      refreshToken: null,
    };
    this.instance = instance;
  }

  private onRequest = (
    config: ICustomInternalAxiosRequestConfig
  ): ICustomInternalAxiosRequestConfig => {
    if (config.headers) {
      Object.assign(config.headers, {
        Authorization: `Bearer ${this.authConfigs.accessToken}`,
      });
    }
    return config;
  };

  private onRequestError = (error: AxiosError): Promise<AxiosError> => {
    console.error(`[request error] [${JSON.stringify(error)}]`);
    return Promise.reject(error);
  };

  private onResponse = (response: AxiosResponse): AxiosResponse => {
    console.info(`[response] [${JSON.stringify(response)}]`);
    return response;
  };

  private onResponseError = (error: AxiosError): Promise<AxiosError> => {
    console.error(`[response error] [${JSON.stringify(error)}]`);
    return Promise.reject(error);
  };

  private setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
    axiosInstance.interceptors.request.use(this.onRequest, this.onRequestError);
    axiosInstance.interceptors.response.use(
      this.onResponse,
      this.onResponseError
    );
    return axiosInstance;
  }

  public async get<T>(url: string, config?: IHttpRequestConfig) {
    return await this.instance.get<T>(url, config);
  }

  public async post<T>(url: string, data?: T, config?: IHttpRequestConfig) {
    return await this.instance.post<T>(url, data, config);
  }

  public async patch<T>(url: string, data: T, config?: IHttpRequestConfig) {
    return await this.instance.patch(url, data, config);
  }

  public async delete(url: string, config?: IHttpRequestConfig) {
    return await this.instance.delete(url, config);
  }

  public getAuthConfigs() {
    return this.authConfigs;
  }

  public setHttpConfigs(config?: Partial<IHttpRequestConfig>) {
    if (config?.baseURL) {
      this.instance.defaults.baseURL = config.baseURL;
    }
  }

  public setAuthConfigs(authConfigs: IAuthConfigs) {
    this.authConfigs = Object.assign(this.authConfigs, authConfigs);
  }
}
