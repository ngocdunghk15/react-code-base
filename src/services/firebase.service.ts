import { initializeApp } from "firebase/app";
import { firebaseConfigs } from "@configs/firebase.configs";
import { getAnalytics } from "firebase/analytics";
import { createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { getStorage } from "firebase/storage";
import { getDatabase } from "firebase/database";
import { errorMessage } from "@configs/error-message.config";

class FirebaseService {
  private firebaseApp;
  private firebaseAnalytics;
  private firebaseAuth;
  private firebaseStorage;
  private firebaseDatabase;
  constructor() {
    this.firebaseApp = initializeApp(firebaseConfigs);
    this.firebaseAnalytics = getAnalytics(this.firebaseApp);
    this.firebaseAuth = getAuth(this.firebaseApp);
    this.firebaseStorage = getStorage(this.firebaseApp);
    this.firebaseDatabase = getDatabase(this.firebaseApp);
  }

  public getFirebaseAuth() {
    return this.firebaseAuth;
  }

  public async createNewAccount(email: string, password: string) {
    return await createUserWithEmailAndPassword(this.firebaseAuth, email, password).catch((error: any) => {
      const { code } = error;
      console.log({ ErrorMessage: errorMessage[code] });
    });
  }

  public async signIn(email: string, password: string) {
    return await signInWithEmailAndPassword(this.firebaseAuth, email, password).catch((error: any) => {
      const { code } = error;
      console.log({ ErrorMessage: errorMessage[code] });
    });
  }

  public async signOut() {
    return await signOut(this.firebaseAuth).catch((error: any) => {
      const { code } = error;
      console.log({ ErrorMessage: errorMessage[code] });
    });
  }

  public async onAuthStateChanged() {
    return new Promise((resolve, reject) => {
      this.firebaseAuth.onAuthStateChanged((userState) => {
        if (userState) {
          resolve(userState);
        } else {
          reject(new Error("Auth State Changed failed"));
        }
      });
    });
  }
}

const firebaseService = new FirebaseService();

export default firebaseService;
