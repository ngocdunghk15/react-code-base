import { API_BASE_URL, API_TIME_OUT } from "@configs/app.configs";

import Http from "./http.service";

export class ApiBaseService {
  protected httpClient: Http;

  constructor() {
    console.log("Environment", process.env.NODE_ENV);
    this.httpClient = new Http({
      baseURL: API_BASE_URL,
      timeout: API_TIME_OUT,
    });
  }
}

export const apiBaseService = new ApiBaseService();
