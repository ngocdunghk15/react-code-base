import React from "react";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { AuthProvider } from "@providers/AuthProvider";
import { AppRouter } from "./routers/AppRouter";

const App: React.FC = () => {
  return (
    <AuthProvider>
      <BrowserRouter>
        <AppRouter />
      </BrowserRouter>
    </AuthProvider>
  );
};

export default App;
