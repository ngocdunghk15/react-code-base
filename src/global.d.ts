interface IAuthConfigs {
  accessToken: string | null;
  refreshToken: string | null;
}

interface IWrapperElement {
  children: JSX.Element;
}
