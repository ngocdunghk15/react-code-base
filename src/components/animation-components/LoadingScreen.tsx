import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

export const LoadingScreen = () => {
  return (
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        height: "100vh",
        justifyContent: "center",
        width: 1,
      }}
    >
      <CircularProgress />
    </Box>
  );
};
