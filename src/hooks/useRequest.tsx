import { useCallback, useState } from "react";

type RequestStatus = "idle" | "pending" | "resolved" | "rejected";

export function useRequest<IPayload = unknown>({
  request,
  onResolve,
  onReject,
}: {
  request: (payload: IPayload) => Promise<unknown>;
  onResolve?: (value: unknown) => void;
  onReject?: (error: unknown) => void;
}) {
  const [status, setStatus] = useState<RequestStatus>("idle");
  const onRequest = useCallback(
    (payload: IPayload) => {
      setStatus("pending");
      request(payload)
        .then((responseData) => {
          setStatus("resolved");
          onResolve?.(responseData);
        })
        .catch((error) => {
          setStatus("rejected");
          onReject?.(error);
        });
    },
    [request, onResolve, onReject]
  );
  return [onRequest, status] as const;
}
