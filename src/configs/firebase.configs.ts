export const firebaseConfigs = {
  apiKey: import.meta.env.VITE_REACT_FIREBASE_API_KEY,
  appId: import.meta.env.VITE_REACT_FIREBASE_APP_ID,
  authDomain: import.meta.env.VITE_REACT_FIREBASE_AUTH_DOMAIN,
  measurementId: import.meta.env.VITE_REACT_FIREBASE_MEASUREMENT_ID,
  messagingSenderId: import.meta.env.VITE_REACT_FIREBASE_SENDER_ID,
  projectId: import.meta.env.VITE_REACT_FIREBASE_PROJECT_ID,
  storageBucket: import.meta.env.VITE_REACT_FIREBASE_STORAGE_BUCKET,
};
