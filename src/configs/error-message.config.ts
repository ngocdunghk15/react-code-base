export const errorMessage: Record<string, string> = {
  'auth/email-already-in-use': 'Email is already in use!',
}
