import { useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "@providers/AuthProvider";

const Lounge = () => {
  const { account, setAccount }: any = useAuthContext();
  const navigate = useNavigate();

  const logout = useCallback(
    (e: any) => {
      e.preventDefault();
      setAccount(null);
      navigate("/");
    },
    [setAccount]
  );

  return (
    <div>
      <h1>Lounge page</h1>
      <p>
        Hello <strong>{account?.username}</strong>!
      </p>
      <p>Looks like you have access to this private route!</p>
      <button onClick={logout}>Logout</button>
    </div>
  );
};

export default Lounge;
