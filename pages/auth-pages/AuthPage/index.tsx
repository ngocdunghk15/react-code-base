import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { LoadingScreen } from "@components/animation-components/LoadingScreen";

const LoginPage = React.lazy(() => import("@pages/auth-pages/LoginPage"));
const NotFoundPage = React.lazy(() => import("@pages/error-pages/NotFoundPage"));

const AuthPage = () => {
  console.log("ZZZ");
  return (
    <Suspense fallback={<LoadingScreen />}>
      <Routes>
        <Route index path={"/login"} />
        <Route path={"/login"} element={<LoginPage />} />
        <Route path={"*"} element={<NotFoundPage />} />
      </Routes>
    </Suspense>
  );
};

export default AuthPage;
