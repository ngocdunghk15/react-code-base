import React, { Suspense } from "react";
import { LoadingScreen } from "@components/animation-components/LoadingScreen";

const LoginForm = React.lazy(() => import("@components/auth-components/LoginForm"));

const LoginPage = () => {
  return (
    <Suspense fallback={<LoadingScreen />}>
      <LoginForm />
    </Suspense>
  );
};

export default LoginPage;
